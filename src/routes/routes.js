import VueRouter from 'vue-router';

const router = new VueRouter({
    routes: [{
            path: '/',
            component: require('../components/Employee-info.vue').default
        },
        {
            path: '/employee-detail',
            component: require('../components/Employee-info.vue').default
        },
        {
            path: '/use-credit/:empid',
            component: require('../components/use-credit.vue').default
        }
    ]
});

export default router;