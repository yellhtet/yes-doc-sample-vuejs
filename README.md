# yes-doc-sample-vuejs

Vuejs with vuetify 

### Get Started

#### Requirements

- Vue.js > 2.x

#### Launch 🚀

- `git clone https://yellhtet@bitbucket.org/yellhtet/yes-doc-sample-vuejs.git`
- `npm install`
- `npm run serve`

----

### Links

- [Vue](https://vuejs.org/v2/api/)
- [Vue Router](https://router.vuejs.org/api/)
- [Vuetify](https://vuetifyjs.com/en/getting-started/quick-start)